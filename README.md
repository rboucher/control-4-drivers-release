# README #
----
## Authentication ##
All of our drivers support a 21 day trial which is activated as soon as a driver is added to a project, whether or not you have an authentication code. If you do have an authentication code simply enter the code and the driver will acknowledge that. Your driver will then use that key instead of the trial system for authorization. You can insert the authentication code at anytime during or after installation. 

## Downloading ##
I highly suggest downloading the files straight from the repository itself. If you know about git simply pull them down into a local copy of the repository. You can also simply go to Source, select a file, and in the upper right side of the source view there's a "raw" button, simply right click, "Save Link As", and download it.

## How to Contribute ##
Have you found an issue with one of our drivers? If so please feel free to post that in the issues section.
Do you have features that you wish to be added to a specific driver? Please feel free to post an issue as a proposal and we'll take a look at it.

## Contact ##
* Development: ** ryanboucher.dev@gmail.com **
* Pricing: ** s.stogrin@theoryav.com **